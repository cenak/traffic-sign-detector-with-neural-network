# FAST-ER - Key points

import cv2
from matplotlib import pyplot as plt

cap = cv2.VideoCapture("VideoFromCar1.mp4")

cv2.namedWindow("Camera")

while 1:
    ret, frame = cap.read()
    cv2.imshow("Camera",frame)
    
    grayFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    #Otsu's thresh
    high_thresh, thresh_im = cv2.threshold(grayFrame, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    lowThresh = 0.5*high_thresh

    edgeFrame =  cv2.Canny(grayFrame,lowThresh,high_thresh) #Read about Canny's threshold

    cv2.imshow("Cam Otsu",edgeFrame)

    
    #edgeFrame =  cv2.Canny(grayFrame,150,250) #Read about Canny's threshold
    
    #cv2.imshow("Cam 100 200",edgeFrame)

    cv2.waitKey(27)
