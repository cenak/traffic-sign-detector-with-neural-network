import numpy as np
import cv2
from matplotlib import pyplot as plt

signImg = cv2.imread("Signs\\Original\\5_19_2.png")

graySignImg = cv2.cvtColor(signImg, cv2.COLOR_BGR2GRAY)

orb = cv2.ORB_create()
kp = orb.detect(graySignImg, None)

kp,des = orb.compute(graySignImg,kp)

graySignImg = cv2.drawKeypoints(graySignImg, kp, graySignImg, color=(0,255,0), flags = 0)

cv2.imwrite("Signs\\kpImg\\5_19_2.png",graySignImg)



while 1:
    cv2.imshow("Sign", signImg)

    cv2.imshow("KeyPoints", graySignImg)

    cv2.waitKey(27)

