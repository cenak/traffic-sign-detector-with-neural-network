import pandas as pd
import numpy as np
import cv2
import os

class BBOX:
    def __init__(self, left, top, right, bottom):
        self.left = left
        self.top = top
        self.right = right
        self.bottom = bottom		


class Sign:
    def __init__(self, refImg, assurance, class_number, termination_time = -1):
        self.refImg = refImg
        self.assurance = assurance
        self.class_number = class_number
        self.termination_time = termination_time

    """ Old Constructor
    def __init__(self, size, class_number):
        self.size = size	# Means height*width
        self.class_number = class_number
    """
	
class GPS_Data:
    def __init__(self, latitude, longitude, timeOfMeasurement):
        self.latitude = latitude
        self.longitude = longitude
        self.timeOfMeasurement = timeOfMeasurement
	
class GPS_Cross_Roads_Thresholds:
    def __init__(self, latitude_min, latitude_max, longitude_min, longitude_max):
        self.latitude_min = latitude_min
        self.latitude_max = latitude_max
        self.longitude_min = longitude_min
        self.longitude_max = longitude_max

class Road:
    def __init__(self, startLat, startLong, endLat, endLong):
        self.startLat = startLat
        self.startLong = startLong
        self.endLat = endLat
        self.endLong = endLong
		
class CrossRoad:
    def __init__(self, Latitude, Longitude):
        self.Latitude = Latitude
        self.Longitude = Longitude
		
class DB_Connector:
    def __init__(self, path_to_GPS_DB):
        self.path_to_GPS_DB = path_to_GPS_DB
        self.data = pd.read_csv(path_to_GPS_DB, ",")
		
        self.roads = []
        self.crossroads = []
		
        for v in self.data.itertuples():
            if v.type == "road":
                self.roads.append(Road(v.startLat,v.startLong,v.endLat,v.endLong))
            elif v.type == "cross":
                self.crossroads.append(CrossRoad(v.startLat,v.startLong))
		
    def getDataFromDB(self):
        return self.data
		
    def getCrossroads(self):
        return self.crossroads
		
    def getRoads(self):
        return self.roads
		
    def getAllFromDB(self):
        return self.roads, self.crossroads
	
	

def get_dataset(data):
    images = []
    labels = []
    for v in data.itertuples():
        image_path = os.path.join("Signs","Reference","{}.png".format(v.sign_class))
        
        if os.path.exists(image_path):
            image = cv2.imread(image_path)
            #image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            scaled_img = cv2.resize(image, ((71,71,3)[:2]))
            images.append(scaled_img)
            labels.append(int(v.class_number))
            print("Added sign with class number {} to references\n".format(v.class_number))
		
        else:
            image = cv2.imread(os.path.join("Signs","Reference","SRDE.png"))
            #image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            scaled_img = cv2.resize(image, ((71,71,3)[:2]))
            images.append(scaled_img)
            labels.append(int(v.class_number))
            print("Image of sign with class number {} doesn't exist (sign class {})\n".format(v.class_number, v.sign_class))
    
    return np.array(images, dtype="float32")/255.0, labels
	
def draw_signs(frame, height, width, actual_signs):
    if len(actual_signs) > 0:
        heightRef, widthRef, _ = actual_signs[0].refImg.shape
	
        number_of_sign = 0
	
        for sign in actual_signs:
            """
            hsv = cv2.cvtColor(sign.refImg, cv2.COLOR_BGR2HSV)
            hsv[:,:,2] = 255
            sign.refImg = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
            """
			
            uintSignImage = np.uint8(sign.refImg)
			
            uintSignImage *= 1500
			
            if ((widthRef+10) * number_of_sign + widthRef + 10) < width:
                frame[height-heightRef-25:height-25, (widthRef+10) * number_of_sign + 10:(widthRef+10) * number_of_sign + widthRef+10] = uintSignImage[:]
                cv2.putText(frame,"{:0.2f}%".format(sign.assurance),((widthRef+10)*number_of_sign+10,height-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5,(0,0,255),1,cv2.LINE_AA)
			
            print("Drawn sign with class {}".format(sign.class_number))
			
            number_of_sign += 1
	
        print("Drawn {} signs\n".format(number_of_sign))
	
    return frame

def get_GPS_data():
    print("Starting reading file with GPS data\n")
	
    # LOC_LALO: timestamp, широта, долгота
	
    # latitude - широта
	# longitude - долгота
	
    GPS_data = []
	
    file = open(os.path.join("VideoForTest","sensors_data-190708-030437.txt"), "r")
	
    for line in file:
        if line.startswith("LOC_LALO"):
            print('Uploaded string: "{}"'.format(line))
            
            line = line.replace("LOC_LALO:", "")
            line = line.replace(",", ".")
			
            data = line.split()
			
            print("Time of measurement: {}".format(data[0]))
            print("Latitude: {}".format(data[1]))
            print("Longitude: {}\n".format(data[2]))
		
            GPS_data.append(GPS_Data(latitude = data[1], longitude = data[2], timeOfMeasurement = data[0]))
	
    file.close()
	
    return GPS_data
	
def check_crossroad(gps_data, frame_counter, thresholds):
    """
	LOC_LALO:1562587797985 55,8020059803119 37,7989563601435
	
	crossroads:
	
    55.799257, 37.793754 first
	55.799197, 37.787989 second
	55.802493, 37.787785 third
	55.802561, 37.793646 fouth
	55.802632, 37.798952 fifth
	55.799318, 37.799127 sixth
	
	first thresholds:
	55.799203, 37.793900
	55.799308, 37.793635
	
	second thresholds:
	55.799127, 37.788133
	55.799259, 37.787814
	
	third thresholds:
	55.802378, 37.787957
	55.802554, 37.787615
	
	fouth thresholds:
	55.802475, 37.793791
	55.802626, 37.793497
	
	fifth thresholds:
	55.802538, 37.799311
	55.802689, 37.798578
	
	sixth thresholds:
	55.799254, 37.799302
	55.799405, 37.798945
	
    """

    """
    thresholds = []
	#threshold for the first crossroad
    thresholds.append(GPS_Cross_Roads_Thresholds(55.799203, 55.799308, 37.793635, 37.793900))

	#threshold for the second crossroad
    thresholds.append(GPS_Cross_Roads_Thresholds(55.799127, 55.799259, 37.787814, 37.788133))
	
	#threshold for the third crossroad
    thresholds.append(GPS_Cross_Roads_Thresholds(55.802378, 55.802554, 37.787615, 37.787957))
	
	#threshold for the fouth crossroad
    thresholds.append(GPS_Cross_Roads_Thresholds(55.802475, 55.802626, 37.793497, 37.793791))
	
	#threshold for the fifth crossroad
    thresholds.append(GPS_Cross_Roads_Thresholds(55.802538, 55.802689, 37.798578, 37.799311))
	
	#threshold for the sixth crossroad
    thresholds.append(GPS_Cross_Roads_Thresholds(55.799254, 55.799405, 37.798945, 37.799302))
    """
    """
	# -------------- Temporary solution ---------------------
    if frame_counter == 2646 or frame_counter == 3655 or frame_counter == 6132 or frame_counter == 6992 or frame_counter == 9092 or frame_counter == 10405:
        return True
	# -------------------------------------------------------
    """
	
    for threshold in thresholds:
        if float(gps_data.latitude) > threshold.latitude_min and float(gps_data.latitude) < threshold.latitude_max and float(gps_data.longitude) > threshold.longitude_min and float(gps_data.longitude) < threshold.longitude_max:
            return True
    
    return False
	
	
	
	
	
	
	
	
	
	
	
	