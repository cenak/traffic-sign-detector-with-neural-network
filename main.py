import cv2
import os
import numpy as np
import pandas as pd
import tensorflow as tf

from PIL import Image
from yolo import YOLO
from auxiliaryModule import BBOX, Sign, GPS_Data, GPS_Cross_Roads_Thresholds, get_dataset, draw_signs, get_GPS_data, check_crossroad

"""
" 1) найти знаки --------------------------------------------------------------------------- Done
"
" 2) понять что мы проехали ---------------------------------------------------------------- Done
"
" 3) понять что за знак -------------------------------------------------------------------- Done
"  3.1) поменять возвращение из detect_iamge с изображения на массив bounding boxes -------- Done
"  3.2) создать и натренировать сеть классификации ----------------------------------------- Done
"  3.3) вставить сеть в программу ---------------------------------------------------------- Done
"  3.4) Определить знак -------------------------------------------------------------------- Done
"
" 4) понять сколько он действует ----------------------------------------------------------- Done
"
" 5) запомнить что он действует ------------------------------------------------------------ Done
"
" 5) понять что он закончился (по GPS, перекресткам, другим знакам) ------------------------ Done
"
" 6) выводить на экран действующие знаки --------------------------------------------------- Done
"  6.1) Массив действующих знаков и его заполнение ----------------------------------------- Done
"  6.2) Выводить на экран референс знака --------------------------------------------------- Done
"  6.3) Выводить уверенность в знаке ------------------------------------------------------- Done
"  6.4) Выводить словесное название знака (?) ---------------------------------------------- 
"
" 7) Трекать знак -------------------------------------------------------------------------- None
"
"
"
"
" Может быть запоминать какой знак видели на 10 секунд, по прошествии десяти секунд удалять его из памяти?
" Это должно помочь не подавать один и тот же знак на трекание несколько раз
" Или присваивать этому знаку уникальный номер и удалять его если он исчез из поля зрения
"
"
"
"
" ОБРЕЗАТЬ ИЗОБРАЖЕНИЕ СЛЕВА И СНИЗУ Т.К. YOLO ПРИНИМАЕТ 416х416 И СЖИМАЕТ ИСХОДНОЕ ИЗОБРАЖЕНИЕ --- Done
"
" СОЗДАТЬ СВОЮ МОДЕЛЬ
"
" ОПТИМИЗИРОВАТЬ ОБРАБОТКУ В DETECT_IMAGE
"""

# Main variables
writeVideo = True
WORK_WITH_GPS = False

#Preparings

yolo = YOLO(model_path="trained_weights_final.h5", anchors_path="model_data/yolo_anchors.txt", 
            classes_path="model_data/trsd_classes.txt", score=0.3)

IMG_SHAPE = (71,71,3)

"""
base_model = tf.keras.applications.xception.Xception(include_top=False, weights='imagenet',
                                                input_shape=IMG_SHAPE, classes=NUM_CLASSES)

x = base_model.output
x = tf.keras.layers.Flatten()(x)
predictions = tf.keras.layers.Dense(NUM_CLASSES, activation=tf.nn.softmax)(x)

model = tf.keras.Model(inputs=base_model.input, outputs=predictions)

model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])"""

			  
choose_model = ['xception_RTSD(1_epoch).h5','xception_RTSD(3_epochs).h5']
			  
			  
recognize_model = tf.keras.models.load_model(os.path.join('SignRecognitionWeights',choose_model[1]))

#tracker = cv2.TrackerKCF_create()
#multiTracker = cv2.MultiTracker_create()

dataFolder = "VideoForTest"

chooseVideo = ["video-190708-030437_1.mp4","cut1.mp4","cut2.mp4","cut3.mp4", "VideoFromCar1.mp4", "VID_20190715_205717_Trim.mp4", "VideoKorolyov.mp4", "VideoKorolyov2.mp4"]

choosedVideo = 6

fileToOpen = os.path.join(dataFolder,chooseVideo[choosedVideo])

if WORK_WITH_GPS:
    GPS_data = get_GPS_data() # length = 71473
    print("length GPS data array:",len(GPS_data))
    fileToOpen = os.path.join(dataFolder,chooseVideo[0])

actual_signs = []
references_images = []
references_labels = []

# Uploading references ---------------------

data = pd.read_csv(os.path.join("datasets","rtsd","numbers_to_classes.csv"), ",")
references_images, references_labels = get_dataset(data)

# ------------------------------------------

# Uploading crossroads thresholds ----------

path_to_CR_DB = os.path.join("Coordinates","CoordinatesCrossroads.csv")
db_crossroads = pd.read_csv(path_to_CR_DB, ",")

thresholdsCR = []

for v in db_crossroads.itertuples():
    thresholdsCR.append(GPS_Cross_Roads_Thresholds(v.latitude_min, v.latitude_max, v.longitude_min, v.longitude_max))

# ------------------------------------------

path_to_GPS_DB = os.path.join("Coordinates","CoordinatesDB.csv")

if writeVideo:
    video_writer = cv2.VideoWriter("ProcessedVideo.mp4", cv2.VideoWriter_fourcc(*'MP4V'), 20, (1920,1080))

frame_counter = 0

"""
cv2.imshow("Sign Reference",references_images[9])

actual_signs.append(Sign(refImg = references_images[9], assurance = 100, class_number = 9))
"""

cap = cv2.VideoCapture(fileToOpen)

ret, frame = cap.read()

height, width, _ = frame.shape
heightORF = int(height / 2) + int(height * 0.22)
widthORF = width - (int(width / 2) - int(width * 0.05))

print("Size of full image: {}x{}".format(height, width))
print("Size of recognition space: {}x{}\n".format(heightORF, widthORF))

#720x960
#518x528

cv2.namedWindow("Camera")

#Main loop and calculatings
while 1:
	# frame - Height 960, Width 720
    ret, frame = cap.read()
	
    if ret == False:
        break
	
    signsImg = []
    bboxes = []
	
    recognition_frame = frame[0:int(height/2)+int(height*0.22),int(width/2)-int(width*0.05):width]
	
    imFrame = Image.fromarray(recognition_frame)
	
    bboxes, predictedFrame = yolo.detect_image(imFrame)
	
    if(len(bboxes) > 0):
		
        whichSignInArea = 0
        workWithThisSigns = []
		
        for box in bboxes:
            signImg = recognition_frame[box.top:box.bottom, box.left:box.right]
            
            signImg = cv2.cvtColor(signImg, cv2.COLOR_BGR2RGB)
            scaled_signImg = cv2.resize(signImg, (IMG_SHAPE[:2]))
			
            signsImg.append(np.array(scaled_signImg, dtype="float32"))
			
            cv2.imshow("Last detected sign",scaled_signImg)
			
            if box.left >= int(widthORF/2):
                workWithThisSigns.append(whichSignInArea)
			
            whichSignInArea += 1
        
        signsThatShouldBePredicted = []
		
        for i in range(len(workWithThisSigns)):
            signsThatShouldBePredicted.append(signsImg[workWithThisSigns[i]])
		
        if len(signsThatShouldBePredicted) > 0:
            predictions = recognize_model.predict(np.array(signsThatShouldBePredicted)/255)
		
            for predicted_sign in predictions:
                category_array = range(len(predicted_sign))
                true_result_id = np.argmax(predicted_sign)
            
                existThisSignInArray = 0
			
			    # Если точность выше такого-то порога то работаем дальше, иначе continue
                if np.max(predicted_sign)*100 > 97:
                    for sign in actual_signs:
                        if sign.class_number == category_array[true_result_id]:
                            existThisSignInArray = 1
                            break

                    #Если знака ещё нет, то добавляем его в массив и рисуем на экране
                    if existThisSignInArray == 0:
                        if category_array[true_result_id] == 79 or category_array[true_result_id] == 80:
                            actual_signs.append(Sign(refImg = references_images[category_array[true_result_id]], assurance = np.max(predicted_sign)*100, class_number = category_array[true_result_id], termination_time = frame_counter + 30))
                        elif category_array[true_result_id] == 17:
                            actual_signs.append(Sign(refImg = references_images[category_array[true_result_id]], assurance = np.max(predicted_sign)*100, class_number = category_array[true_result_id], termination_time = frame_counter + 60))
							
							#Разрешение только некоторых знаков, если нужны все то заменить на else
                        elif category_array[true_result_id] == 44 or category_array[true_result_id] == 50 or category_array[true_result_id] == 77 or category_array[true_result_id] == 24 or category_array[true_result_id] == 84 or category_array[true_result_id] == 18: 
                            actual_signs.append(Sign(refImg = references_images[category_array[true_result_id]], assurance = np.max(predicted_sign)*100, class_number = category_array[true_result_id]))
                
                        print("Added sign with class: {}".format(category_array[true_result_id]))
                        print("Assurance: {}\n".format(np.max(predicted_sign)*100))
			
                    else:
                        print("Sign with class {} are actual already\n".format(category_array[true_result_id]))
			
            #else:
            #    continue
			

        """ Старая версия
				
        count_of_loop_passes = 0
		

		# Если отслеживаемый знак исчез с правой стороны, то добавляем его в массив действующих знаков и убираем из массива отслеживаемых
        for predicted_sign in predictions:
            category_array = range(len(predicted_sign))
            true_result_id = np.argmax(predicted_sign)
			
            size_s = (bboxes[count_of_loop_passes].bottom - bboxes[count_of_loop_passes].top) * (bboxes[count_of_loop_passes].right - bboxes[count_of_loop_passes].left)
			
            existThisSignInArray = 0
            checked_sign_index = 0
			
            for sign in tracked_signs:
                if sign.class_number == category_array[true_result_id]:
                    if sign.size >= size_s:
                        existThisSignInArray = 1
                        break
                    else:
                        existThisSignInArray = 2
                        break
				
                checked_sign_index += 1
            """
        """
            if existThisSignInArray == 0:
                tracked_signs.append(Sign(size = size_s, class_number = category_array[true_result_id]))
                #Добавить в отслеживаемый
				
                box = (bboxes[count_of_loop_passes].left, bboxes[count_of_loop_passes].top, bboxes[count_of_loop_passes].right - bboxes[count_of_loop_passes].left, bboxes[count_of_loop_passes].bottom - bboxes[count_of_loop_passes].top)
				#(left, top, length from left to right,length from top to bot)
                multiTracker.add(cv2.TrackerKCF_create(), recognition_frame, box)
				
                print("Added sign with class:", category_array[true_result_id], " and size =", size_s)
                print("\nAssurance:", np.max(predicted_sign)*100)
			
            elif existThisSignInArray == 2:
                tracked_signs.pop(checked_sign_index)
				#Добавить в отслеживаемый и убрать старый
                del multiTracker
				
                multiTracker = cv2.MultiTracker_create()
                tracked_signs.append(Sign(size = size_s, class_number = category_array[true_result_id]))
                box = (bboxes[count_of_loop_passes].left, bboxes[count_of_loop_passes].top, bboxes[count_of_loop_passes].right - bboxes[count_of_loop_passes].left, bboxes[count_of_loop_passes].bottom - bboxes[count_of_loop_passes].top)
                multiTracker.add(cv2.TrackerKCF_create(), recognition_frame, box)
				
                print("Replaced sign with class:", category_array[true_result_id])

            else:
                print("Sign with class =", category_array[true_result_id], " exist in array tracked_signs")
			
            count_of_loop_passes += 1
        """
        #break #Finish program
			
			# Using signsImg
			# Sends it to recognizer
		
    recognition_frame = np.array(predictedFrame)
    
    #_, boxes = multiTracker.update(recognition_frame)
	
    """
    for i, newbox in enumerate(boxes):
        p1 = (int(newbox[0]), int(newbox[1]))
        p2 = (int(newbox[0] + newbox[2]), int(newbox[1] + newbox[3]))
        cv2.rectangle(recognition_frame, p1, p2, (0,255,0), 2, 1)
    """
	
    # ------------------------------------- Work with GPS -----------------------------
    if WORK_WITH_GPS:
        sel_gps_data = round(frame_counter * 6.5) # между 6 и 6,5. 6 запаздывает, 6,5 немного обгоняет
        print("Current position: latitude = {} longtitude = {}\n".format(GPS_data[sel_gps_data].latitude,GPS_data[sel_gps_data].longitude))
        cv2.putText(frame,str(GPS_data[sel_gps_data].latitude),(120,20), cv2.FONT_HERSHEY_SIMPLEX, 0.7,(0,0,255),2,cv2.LINE_AA)
        cv2.putText(frame,str(GPS_data[sel_gps_data].longitude),(120,50), cv2.FONT_HERSHEY_SIMPLEX, 0.7,(0,0,255),2,cv2.LINE_AA)
		
        if check_crossroad(GPS_data[sel_gps_data], frame_counter, thresholdsCR):
            actual_signs.clear()
    
    elif choosedVideo == 5: # Korolyov 1
        if frame_counter == 1600 or frame_counter == 4102 or frame_counter == 6490 or frame_counter == 8228:
            actual_signs.clear()

    elif choosedVideo == 6: # Korolyov 2 Rain
        if frame_counter == 2717 or frame_counter == 7018 or frame_counter == 7962 or frame_counter == 10960 or frame_counter == 13532:
            actual_signs.clear()
			
    elif choosedVideo == 7: # Korolyov 3 Rain
        if frame_counter == 4127 or frame_counter == 6299:
            actual_signs.clear()
        
    # ----------------------------------- Work with GPS end ---------------------------


	#Detection WorkSpace
    cv2.putText(recognition_frame,'Detection WorkSpace',(10,20), cv2.FONT_HERSHEY_SIMPLEX, 0.7,(0,0,255),2,cv2.LINE_AA)
    cv2.line(recognition_frame, (2, 0), (2, heightORF), (0,0,255), 2)
    cv2.line(recognition_frame, (2, heightORF-2), (int(widthORF/2), heightORF-2), (0,0,255), 2)
	
    #Recognition WorkSpace
    cv2.putText(recognition_frame,'Recognition WorkSpace',(int(widthORF/2)+6,20), cv2.FONT_HERSHEY_SIMPLEX, 0.7,(0,255,0),2,cv2.LINE_AA)
    cv2.line(recognition_frame, (int(widthORF/2), 0), (int(widthORF/2), heightORF), (0,255,0), 2)
    cv2.line(recognition_frame, (int(widthORF/2), heightORF-2), (widthORF, heightORF-2), (0,255,0), 2)
	
	# Frame counter
    cv2.putText(frame,str(frame_counter),(10,20), cv2.FONT_HERSHEY_SIMPLEX, 0.7,(0,0,255),2,cv2.LINE_AA)
	
    frame[0:int(height/2)+int(height*0.22),int(width/2)-int(width*0.05):width] = recognition_frame[:]
	
    index = 0
    for sign in actual_signs:
        if (sign.class_number == 79 or sign.class_number == 17 or sign.class_number == 80) and frame_counter == sign.termination_time:
            actual_signs.pop(index)
        index += 1
	
    frame = draw_signs(frame, height, width, actual_signs)
	
    cv2.imshow("Camera",frame)
    #cv2.imshow("Recognition WorkSpace",recognition_frame)
	
    if writeVideo:
        video_writer.write(frame)
    
    frame_counter += 1
	
    cv2.waitKey(27)

cap.release()
if writeVideo:
    video_writer.release()
cv2.destroyAllWindows()